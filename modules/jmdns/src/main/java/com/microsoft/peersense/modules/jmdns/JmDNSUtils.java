package com.microsoft.peersense.modules.jmdns;

import javax.jmdns.ServiceEvent;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

/**
 * Created by shubham on 4/18/17.
 */
class JmDNSUtils {

    static JmDNSConnection getEventGenerator(ServiceEvent event) {
        InetAddress address = getSourceAddress(event);
        int port = event.getInfo().getPort();
        return new JmDNSConnection(address, port);
    }

    static boolean isMyAddress(InetAddress address) {
        if (address.isAnyLocalAddress() || address.isLoopbackAddress())
            return true;

        // Check if the address is defined on any interface
        try {
            return NetworkInterface.getByInetAddress(address) != null;
        } catch (SocketException e) {
            return false;
        }
    }

    static String toString(ServiceEvent event) {
        return "address: '"+event.getInfo().getInet4Addresses()[0]+"' name: '"+event.getInfo().getName()+"'";
    }

    static InetAddress getSourceAddress(ServiceEvent event) {
        return event.getInfo().getInet4Addresses()[0];
    }

}
