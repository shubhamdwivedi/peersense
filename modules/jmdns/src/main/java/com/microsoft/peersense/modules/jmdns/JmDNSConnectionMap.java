package com.microsoft.peersense.modules.jmdns;

import java.net.InetAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shubham on 2/22/17.
 */
class JmDNSConnectionMap {

    private Map<InetAddress, JmDNSConnection> addressToConnectionMap;

    JmDNSConnectionMap() {
        addressToConnectionMap = new ConcurrentHashMap<>();
    }

    void put(JmDNSConnection connection) {
        addressToConnectionMap.put(connection.getAddress(), connection);
    }

    JmDNSConnection getConnectionWithAddress(InetAddress address) {
        return addressToConnectionMap.get(address);
    }

    void remove(JmDNSConnection connection) {
        addressToConnectionMap.remove(connection.getAddress());
    }

}
