package com.microsoft.peersense.modules.jmdns;

import com.microsoft.peersense.core.Module;
import com.microsoft.peersense.core.Network;
import com.microsoft.peersense.core.exception.NetworkException;
import com.microsoft.peersense.support.concurrency.Condition;
import com.microsoft.peersense.support.concurrency.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;
import java.io.IOException;
import java.net.*;
import java.util.UUID;

/**
 * Created by shubham on 2/21/17.
 */
public class JmDNSModule extends Module {

    private static final Logger log = LoggerFactory.getLogger(JmDNSModule.class);
    private static JmDNSModule instance;

    private JmDNS jmDNS;
    private ServerSocket mServerSocket;
    private RequestAcceptorThread requestAcceptorThread;
    private InetAddress localAddress;
    private JmDNSConnectionMap connectionMap;
    private String serviceType;

    private JmDNSModule() {
        connectionMap = new JmDNSConnectionMap();
        serviceType = "_" + Network.getInstance().getNetworkId() + "._tcp.local.";
        requestAcceptorThread = new RequestAcceptorThread();
    }

    public static JmDNSModule getInstance() {
        if (instance == null) {
            instance = new JmDNSModule();
        }
        return instance;
    }

    // This is needed on Android for setting the local address. There is no
    // reliable way to determine the local address on Android without using
    // Android's libraries, hence any android client will need to determine
    // the local address and then use this method. Without this JmDNS won't
    // work on Android.
    public void setLocalAddress(InetAddress address) {
        this.localAddress = address;
    }

    @Override
    public void startup() {
        startReceiver();
        startDiscovery();
    }

    private void startReceiver() {
        try {

            mServerSocket = new ServerSocket(0);
            log.debug("Listening on port: " + mServerSocket.getLocalPort());

        } catch (IOException e) {
            throw new NetworkException("Server socket failed to initialise!", e);
        }

        requestAcceptorThread.start();
    }

    private void startDiscovery() {
        try {

            if (localAddress == null) {
                jmDNS = JmDNS.create();
            } else {
                jmDNS = JmDNS.create(localAddress, localAddress.getHostName());
            }

            // Register a service so that others can discover the host.  Giving a random string
            // as service name is crucial for discovery. It ensures that other peers rediscover
            // the host properly and do not return cached values from the previous session.
            String randomServiceName = UUID.randomUUID().toString();
            ServiceInfo serviceInfo = ServiceInfo.create(serviceType, randomServiceName, mServerSocket.getLocalPort(), "");
            jmDNS.registerService(serviceInfo);

            // Discover others on the network
            jmDNS.addServiceListener(serviceType, new ServiceListener() {
                @Override
                public void serviceAdded(ServiceEvent serviceEvent) {
                    jmDNS.requestServiceInfo(serviceEvent.getType(), serviceEvent.getName());
                }

                @Override
                public void serviceRemoved(ServiceEvent serviceEvent) {
                    JmDNSConnection peer = JmDNSUtils.getEventGenerator(serviceEvent);

                    if (!JmDNSUtils.isMyAddress(peer.getAddress())) {
                        log.debug("[REMOVED] " + JmDNSUtils.toString(serviceEvent));
                        connectionMap.remove(peer);
                        onLost(peer);
                    }
                }

                @Override
                public void serviceResolved(ServiceEvent serviceEvent) {
                    JmDNSConnection connection = JmDNSUtils.getEventGenerator(serviceEvent);

                    if (!JmDNSUtils.isMyAddress(connection.getAddress())) {
                        log.debug("[RESOLVED] " + JmDNSUtils.toString(serviceEvent));
                        connectionMap.put(connection);
                        onDiscovered(connection);
                    }
                }
            });

        } catch (IOException e) {
            throw new NetworkException("Failed in setting up discovery", e);
        }
    }

    @Override
    public void shutdown() {
        jmDNS.unregisterAllServices();
        requestAcceptorThread.terminate();
        try {

            mServerSocket.close();
            jmDNS.close();

        } catch (IOException e) {
            log.error("Failed to shutdown JmDNS module", e);
        }
    }

    private class RequestAcceptorThread extends Thread {

        private boolean isRunning = true;

        @Override
        public void run() {
            while (isRunning) {
                try {

                    Socket mSocket = mServerSocket.accept();
                    new RequestHandlerThread(mSocket).start();

                } catch (SocketException e) {

                    if(isRunning)
                        log.error("Request acceptor thread terminated abnormally",e);

                } catch (IOException e) {
                    log.error("Failure in request acceptor thread", e);
                }
            }
        }

        public void terminate() {
            isRunning = false;
        }

    }

    private class RequestHandlerThread extends Thread {

        private Socket mSocket;

        public RequestHandlerThread(Socket socket) {
            this.mSocket = socket;
        }

        @Override
        public void run() {
            try {

                if(getSender() == null) {

                    Wait.until(new Condition("theSenderHasBeenDiscovered") {
                        @Override
                        public boolean isSatisfied() {
                            return connectionMap.getConnectionWithAddress(mSocket.getInetAddress()) != null;
                        }
                    }, 60000);

                }

                onDataReceived(getSender(), mSocket.getInputStream());
                mSocket.close();

            } catch (Exception e) {
                log.error("Failure in handling request", e);
            }
        }

        private JmDNSConnection getSender() {
            return connectionMap.getConnectionWithAddress(mSocket.getInetAddress());
        }

    }

}
