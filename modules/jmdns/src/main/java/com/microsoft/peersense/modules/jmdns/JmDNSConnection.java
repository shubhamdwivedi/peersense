package com.microsoft.peersense.modules.jmdns;

import com.microsoft.peersense.core.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by shubham on 2/18/17.
 */
class JmDNSConnection extends Connection {

    private static final Logger log = LoggerFactory.getLogger(JmDNSConnection.class);

    private InetAddress inetAddress;
    private int port;

    JmDNSConnection(InetAddress inetAddress, int port) {
        this.inetAddress = inetAddress;
        this.port = port;
    }

    @Override
    protected OutputSession createNewSession() throws IOException {
        return new OutputSession() {

            private Socket socket = new Socket(inetAddress, port);

            @Override
            protected OutputStream getOutputStream() throws IOException {
                return socket.getOutputStream();
            }

            @Override
            protected void onComplete() throws IOException {
                socket.close();
            }

        };
    }

    InetAddress getAddress() {
        return inetAddress;
    }

    @Override
    public String toString() {
        return inetAddress.getHostAddress();
    }

}
