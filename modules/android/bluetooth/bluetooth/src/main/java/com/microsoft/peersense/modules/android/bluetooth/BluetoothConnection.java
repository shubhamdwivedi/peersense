package com.microsoft.peersense.modules.android.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.microsoft.peersense.core.Connection;
import com.microsoft.peersense.core.Network;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.UUID;

/**
 * Created by shubham on 4/20/17.
 */

public class BluetoothConnection extends Connection {

    private BluetoothDevice device;

    BluetoothConnection(BluetoothDevice device) {
        this.device = device;
    }

    @Override
    protected OutputSession createNewSession() throws IOException {

        return new OutputSession() {

            private BluetoothSocket socket;

            @Override
            protected void onStart() throws IOException {
                socket = device.createRfcommSocketToServiceRecord(UUID.fromString(Network.getInstance().getHostDeviceId()));
                socket.connect();
            }

            @Override
            protected OutputStream getOutputStream() throws IOException {
                return socket.getOutputStream();
            }

            @Override
            protected void onComplete() throws IOException {
                socket.close();
            }

        };

    }

    BluetoothDevice getDevice() {
        return device;
    }

}
