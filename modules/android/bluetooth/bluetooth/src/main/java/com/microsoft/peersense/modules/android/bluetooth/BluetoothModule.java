package com.microsoft.peersense.modules.android.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.microsoft.peersense.core.Connection;
import com.microsoft.peersense.core.Module;
import com.microsoft.peersense.support.concurrency.Condition;
import com.microsoft.peersense.support.concurrency.Wait;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * Created by shubham on 4/20/17.
 */

public class BluetoothModule extends Module {

    private static final Logger log = LoggerFactory.getLogger(BluetoothModule.class);

    private static final String SECURE_NAME = "SecureBluetoothConnection";
    private static final String INSECURE_NAME = "InsecureBluetoothConnection";
    private static final UUID SECURE_UUID = UUID.fromString(SECURE_NAME);
    private static final UUID INSECURE_UUID = UUID.fromString(INSECURE_NAME);
    private static final boolean SECURE = true;

    private Context context;
    private BluetoothAdapter adapter;
    private BluetoothConnectionMap connectionMap;
    private RequestAcceptorThread secureServerThread, insecureServerThread;

    public BluetoothModule(Context context) {

        this.context = context;
        this.adapter = BluetoothAdapter.getDefaultAdapter();
        this.connectionMap = new BluetoothConnectionMap();
        this.secureServerThread = new RequestAcceptorThread(SECURE);
        this.insecureServerThread = new RequestAcceptorThread(!SECURE);

    }

    @Override
    public void startup() {
        reportPairedDevices();
        startReceiver();

        // For now we are only working with devices that are already paired,
        // so startDiscovery() has been commented out.
        // startDiscovery();
    }

    private void reportPairedDevices() {
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        for(BluetoothDevice device : pairedDevices) {
            onDiscovered(device);
        }
    }

    private void startReceiver() {

        adapter.startDiscovery();
        secureServerThread.start();
        insecureServerThread.start();

    }

    private void startDiscovery() {
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if(device.getBondState() == BluetoothDevice.BOND_BONDED)
                    onDiscovered(device);
            }
        }, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if(device.getBondState() == BluetoothDevice.BOND_NONE || device.getBondState() == BluetoothDevice.ERROR)
                    onLost(device);
            }
        }, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
    }

    @Override
    public void shutdown() {

        adapter.cancelDiscovery();
        secureServerThread.terminate();
        insecureServerThread.terminate();

    }

    private void onDiscovered(BluetoothDevice device) {

        BluetoothConnection newConnection = new BluetoothConnection(device);
        connectionMap.put(newConnection);
        onDiscovered(connectionMap.getConnectionWithAddress(device.getAddress()));

    }

    private void onLost(BluetoothDevice device) {

        Connection lostConnection = connectionMap.getConnectionWithAddress(device.getAddress());
        if(lostConnection != null)
            onLost(lostConnection);

    }

    private class RequestAcceptorThread extends Thread {

        private boolean isRunning;
        private BluetoothServerSocket serverSocket;

        RequestAcceptorThread(boolean isSecure) {
            isRunning = true;

            try {
                if (isSecure)
                    serverSocket = adapter.listenUsingRfcommWithServiceRecord(SECURE_NAME, SECURE_UUID);
                else
                    serverSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(INSECURE_NAME, INSECURE_UUID);
            }
            catch (IOException e) {
                log.error("Failure in establishing server socket", e);
            }

        }


        @Override
        public void run() {
            while (isRunning) {
                try {

                    BluetoothSocket socket = serverSocket.accept();
                    new RequestHandlerThread(socket).start();

                }
                catch (IOException e) {

                    if(isRunning)
                        log.error("Requested acceptor thread terminated abnormally", e);

                }
            }
        }

        void terminate() {
            isRunning = false;
            try {
                serverSocket.close();
            }
            catch (IOException e) {
                log.error("Failed to close server socket", e);
            }
        }

    }

    private class RequestHandlerThread extends Thread {

        private BluetoothSocket socket;

        RequestHandlerThread(BluetoothSocket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {

            try {

                if (getSender() == null) {

                    Wait.until(new Condition("theSenderHasBeenDiscovered") {
                        @Override
                        public boolean isSatisfied() {
                            return getSender() != null;
                        }
                    }, 60000);

                }

                onDataReceived(getSender(), socket.getInputStream());
                socket.close();

            }
            catch (IOException e) {
                log.error("Error in handling request", e);
            }

        }

        private BluetoothConnection getSender() {
            return connectionMap.getConnectionWithAddress(socket.getRemoteDevice().getAddress());
        }

    }

}
