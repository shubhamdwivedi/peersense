package com.microsoft.peersense.modules.android.bluetooth;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shubham on 4/21/17.
 */

public class BluetoothConnectionMap {

    private Map<String, BluetoothConnection> addressToConnectionMap;

    BluetoothConnectionMap() {
        addressToConnectionMap = new ConcurrentHashMap<>();
    }

    void put(BluetoothConnection connection) {
        if(addressToConnectionMap.get(connection.getDevice().getAddress()) == null)
            addressToConnectionMap.put(connection.getDevice().getAddress(), connection);
    }

    BluetoothConnection getConnectionWithAddress(String address) {
        return addressToConnectionMap.get(address);
    }

    void remove(BluetoothConnection connection) {
        addressToConnectionMap.remove(connection.getDevice().getAddress());
    }

}
