# PeerSense #

PeerSense SDK easily enables talking to other devices in the network. It can use multiple comfigurable network modules to achieve that. Currently it uses __JmDNS__ and __Android Bluetooth__ modules. 
We can also have __WiFi P2P__ module for devices that support it.

## Design ##

The core provide a uniform set of interfaces which are implemented by multiple modules. It also provides a set of funcationalities that are common across multiple modules. 
This ensures that there are uniform API's for cross-device communication.

### Core interfaces/libraries ###

* __Connection__ : Provides the `send(Payload)` method. Has an abstract `createNewSession()` method which is given by the implenting module.
* __Protocol__   : This generic class describes how an object of type `T` has to be serialised and deserialised. Has implementations for integers, objects, strings and serializables. 
* __Payload__    : Encapsulates any data that is sent on the network. Has corresponding `PayloadBuilder` and `PayloadReader`. Completely abstracts how data is translated and sent on the network.
* __Module__     : Declares `onDiscovered(Connection)` `onLost(Connection)` `onReceived(Connection, Payload)` methods which should be called as and when requried by the implementing module.
* __Network__    : The singleton that represents the app network. Can take in multiple module implementations.

### Examples ###

Sample implementations for __android__ and __corejava__ are provided which showcase the exact usage.