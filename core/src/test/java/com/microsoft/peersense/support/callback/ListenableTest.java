package com.microsoft.peersense.support.callback;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by shubham on 9/23/16.
 */
public class ListenableTest {

    private SampleListenable listenable;

    @Before
    public void beforeEachTest() {
        listenable = new SampleListenable();
    }

    @Test
    public void shouldUpdateSingleListener() {
        BooleanHolder callbackSuccessful = new BooleanHolder(false);
        listenable.addListener(new MarkerListener());
        listenable.updateListeners(callbackSuccessful);
        Assert.assertEquals(true, callbackSuccessful.getValue());
    }

    @Test
    public void shouldUpdateMultipleListeners() {
        int numberOfListeners = 5;
        IntegerHolder numberOfCallbacksCalled = new IntegerHolder(0);

        for (int i = 0; i < numberOfListeners; i++)
            listenable.addListener(new IncrementListener());

        listenable.updateListeners(numberOfCallbacksCalled);
        Assert.assertEquals(numberOfListeners, numberOfCallbacksCalled.getValue());
    }

    @Test
    public void shouldNotUpdateRemovedListener() {
        MarkerListener listener = new MarkerListener();
        listenable.addListener(listener);
        listenable.removeListener(listener);
        BooleanHolder callbackSuccessful = new BooleanHolder(false);
        listenable.updateListeners(callbackSuccessful);
        Assert.assertFalse(callbackSuccessful.getValue());
    }

    @Test
    public void shouldNotUpdateAnyListenerWhenAllListenersAreRemoved() {
        IntegerHolder numberOfCallbacksCalled = new IntegerHolder(0);
        listenable.addListener(new IncrementListener());
        listenable.addListener(new IncrementListener());
        listenable.removeAllListeners();
        listenable.updateListeners(numberOfCallbacksCalled);
        Assert.assertEquals(0, numberOfCallbacksCalled.getValue());
    }

    private class SampleListenable extends Listenable<Listener> {
    }

    private class IncrementListener implements Listener {
        public void onUpdated(Object... arguments) {
            ((IntegerHolder) arguments[0]).add(1);
        }
    }

    private class MarkerListener implements Listener {
        public void onUpdated(Object... arguments) {
            ((BooleanHolder) arguments[0]).setValue(true);
        }
    }

    private class BooleanHolder {
        boolean value;

        BooleanHolder(boolean value) {
            this.value = value;
        }

        boolean getValue() {
            return value;
        }

        void setValue(boolean value) {
            this.value = value;
        }
    }

    private class IntegerHolder {
        int value;

        IntegerHolder(int value) {
            this.value = value;
        }

        int getValue() {
            return value;
        }

        void add(int increment) {
            value += increment;
        }
    }

}
