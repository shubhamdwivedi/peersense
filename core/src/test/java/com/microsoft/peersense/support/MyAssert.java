package com.microsoft.peersense.support;

import org.junit.Assert;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by shubham on 9/24/16.
 */
public class MyAssert {

    public static void assertAllEqual(Object... objects) {
        if (objects.length > 0) {
            Object first = objects[0];
            for (int i = 1; i < objects.length; i++) {
                Assert.assertEquals(first, objects[i]);
            }
        }
    }

    public static void assertAllDifferent(Object... objects) {
        Set<Object> set = new TreeSet<>();
        for (Object object : objects) {
            set.add(object);
        }
        Assert.assertEquals(objects.length, set.size());
    }

}
