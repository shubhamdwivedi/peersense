package com.microsoft.peersense.support;

import com.google.common.base.Objects;
import com.microsoft.peersense.core.Connection;
import com.microsoft.peersense.core.Module;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by shubham on 9/24/16.
 */
public class Tests {

    public static final String STRING = "Interstellar";
    public static final String LARGE_STRING = multiply(STRING, 10000);
    public static final int INTEGER = 1234546789;
    public static final byte BYTE = 63;
    public static final long LONG = 1234567890123456789L;
    public static final short SHORT = 12345;
    public static final double DOUBLE = 123.45;
    public static final float FLOAT = 123.45f;
    public static final File FILE = new File("src/test/resources/corpus/small.txt");
    public static final File LARGE_FILE = new File("src/test/resources/corpus/large.txt");
    public static final File NON_EXISTING_FILE = new File("/~!@#$%^&*()");
    public static final String TEMP_DIR = System.getProperty("java.io.tmpdir") + "unit_tests";
    public static final TestObject OBJECT = new TestObject(STRING, INTEGER);
    public static final TestObject LARGE_OBJECT = new TestObject(LARGE_STRING, INTEGER);
    public static final Connection CONNECTION = new StubConnection();
    public static final Map<String, String> MAP = new HashMap<String, String>() {{
        put("First", STRING);
        put("Second", STRING);
        put("Third", STRING);
    }};

    private static String multiply(String string, int times) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < times; i++) {
            sb.append(string);
        }
        return sb.toString();
    }

    public static void validatePercentageProgressReport(List<Float> reports) {
        validatePercentageProgressReport(reports, 1);
    }

    public static void validatePercentageProgressReport(List<Float> reports, int minimumSize) {
        // 100% should b reported at last
        assertTrue(reports.get(reports.size() - 1) == 100f);

        // reports should be in non-decreasing order
        boolean shouldBeNonDecreasing = true;
        for (int i = 1; i < reports.size(); i++) {
            shouldBeNonDecreasing = shouldBeNonDecreasing && reports.get(i) >= reports.get(i - 1);
        }
        assertTrue(shouldBeNonDecreasing);

        assertTrue(reports.size() >= minimumSize);

    }

    public static class TestObject implements Serializable {
        public String field1;
        public int field2;

        public TestObject(String field1, int field2) {
            this.field1 = field1;
            this.field2 = field2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestObject that = (TestObject) o;
            return field2 == that.field2 &&
                    Objects.equal(field1, that.field1);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(field1, field2);
        }
    }

    public static class StubConnection extends Connection {

        @Override
        public OutputSession createNewSession() throws IOException {
            return new OutputSession() {
                @Override
                protected OutputStream getOutputStream() throws IOException {
                    return new ByteArrayOutputStream();
                }
            };
        }
    }

    public static class StubModule extends Module {
        @Override
        public void startup() {
        }

        @Override
        public void shutdown() {
        }
    }

}
