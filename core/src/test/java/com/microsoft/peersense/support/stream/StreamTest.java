package com.microsoft.peersense.support.stream;

import com.microsoft.peersense.support.Tests;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by shubham on 4/19/17.
 */
public class StreamTest {

    private static final Logger log = LoggerFactory.getLogger(StreamTest.class);

    private static final int LARGE = rnd(50000, 100000);
    private static final int SMALL = rnd(500, 1000);
    private static final int SMALL_CHUNK_SIZE = rnd(100, 200);
    private static final int LARGE_CHUNK_SIZE = rnd(10000, 20000);

    @BeforeClass
    public static void startup() {
        log.debug("Large:" + LARGE + " Small:" + SMALL + " LargeChunk:" + LARGE_CHUNK_SIZE + " SmallChunk:" + SMALL_CHUNK_SIZE);
    }

    private static int rnd(int min, int max) {
        return min + (int) (Math.random() * (max - min));
    }

    @Test
    public void shouldCopyExactNumberOfBytes() throws Exception {
        int actualCopiedBytes = (int) Stream.copy(input(SMALL), output(), SMALL);
        assertEquals(SMALL, actualCopiedBytes);
    }

    @Test
    public void shouldCopyAvailableNumberOfBytes() throws Exception {
        int actualCopiedBytes = (int) Stream.copy(input(SMALL), output(), LARGE);
        assertEquals(SMALL, actualCopiedBytes);
    }

    @Test
    public void shouldCopyGivenNumberOfBytes() throws Exception {
        int actualCopiedBytes = (int) Stream.copy(input(LARGE), output(), SMALL);
        assertEquals(SMALL, actualCopiedBytes);
    }

    @Test
    public void shouldReportCompletion() throws Exception {
        final List<Float> reports = new ArrayList<>();
        Stream.copy(input(LARGE), output(), LARGE, new PercentageProgressListener() {
            @Override
            public void onPercentageCompleted(float percentageCompleted) {
                reports.add(percentageCompleted);
            }
        });
        Tests.validatePercentageProgressReport(reports);
    }

    @Test
    public void shouldReportWithMaxTwoDecimalPrecision() throws Exception {
        Stream.copy(input(SMALL), output(), SMALL, new PercentageProgressListener() {
            @Override
            public void onPercentageCompleted(float percentageCompleted) {
                String string = percentageCompleted + "";
                int placesAfterDecimal = string.length() - (string.indexOf(".") + 1);
                Assert.assertTrue(placesAfterDecimal <= 2);
            }
        });
    }

    @Test
    public void shouldHaveMoreProgressReportsForSmallerChunkSize() throws Exception {

        final List<Float> smallChunkReports = new ArrayList<>();
        Stream.copy(input(SMALL), output(), SMALL, SMALL_CHUNK_SIZE, new PercentageProgressListener() {
            @Override
            public void onPercentageCompleted(float percentageCompleted) {
                smallChunkReports.add(percentageCompleted);
            }
        });

        final List<Float> largeChunkReports = new ArrayList<>();
        Stream.copy(input(SMALL), output(), SMALL, LARGE_CHUNK_SIZE, new PercentageProgressListener() {
            @Override
            public void onPercentageCompleted(float percentageCompleted) {
                largeChunkReports.add(percentageCompleted);
            }
        });

        Tests.validatePercentageProgressReport(largeChunkReports);
        Tests.validatePercentageProgressReport(smallChunkReports, largeChunkReports.size());
    }

    @Test
    public void shouldOnlyReadSpecifiedNumberOfBytesFromInputStream() throws IOException {
        InputStream input = input(LARGE);
        Assert.assertEquals(LARGE, Stream.available(input));
        Stream.copy(input, output(), SMALL);
        Assert.assertEquals(LARGE - SMALL, Stream.available(input));
    }

    private InputStream createRandomInputStream(int size) {
        byte[] randomBytes = new byte[size];
        (new Random()).nextBytes(randomBytes);
        return new ByteArrayInputStream(randomBytes);
    }

    private InputStream input(int size) {
        return createRandomInputStream(size);
    }

    private OutputStream output() {
        return new ByteArrayOutputStream();
    }

}
