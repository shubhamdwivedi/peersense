package com.microsoft.peersense.support.concurrency;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by shubham on 3/29/17.
 */
public class WaitTest {

    private static long SHORT_PERIOD = 10;
    private static long MEDIUM_PERIOD = 100;

    private boolean flag;
    private Condition condition = new Condition("flagIsTrue") {
        @Override
        public boolean isSatisfied() {
            return flag;
        }
    };

    @Before
    public void beforeEachTest() {
        flag = false;
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowIfConditionNotSatisfied() {
        Wait.until(condition, SHORT_PERIOD);
    }

    @Test(timeout = 500)
    public void shouldContinueIfConditionSatisfied() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Wait.period(SHORT_PERIOD);
                flag = true;
            }
        }).start();
        Wait.until(condition, MEDIUM_PERIOD);
    }
}
