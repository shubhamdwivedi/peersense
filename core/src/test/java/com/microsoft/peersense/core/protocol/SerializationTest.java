package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.Tests;
import junitx.framework.FileAssert;
import lombok.AllArgsConstructor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by shubham on 3/24/17.
 */
@RunWith(Parameterized.class)
@AllArgsConstructor
public class SerializationTest {

    private Protocol protocol;
    private Object testValue;

    @Parameters(name = "test: {0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new ByteProtocol(), Tests.BYTE},
                {new ShortProtocol(), Tests.SHORT},
                {new IntegerProtocol(), Tests.INTEGER},
                {new LongProtocol(), Tests.LONG},
                {new SerializableProtocol(), Tests.DOUBLE},
                {new SerializableProtocol(), Tests.FLOAT},
                {new SerializableProtocol(), Tests.STRING},
                {new SerializableProtocol(), Tests.OBJECT},
                {new SerializableProtocol(), Tests.MAP},
                {new FileProtocol(), Tests.FILE},
                {new FileProtocol(), Tests.LARGE_FILE},
        });
    }

    @Test
    public void shouldReadSameValueThatWasWritten() {
        InputStream stream = protocol.serialize(testValue).getStream();
        Object readValue = protocol.deserialize(stream).getObject();
        assertEquals(testValue, readValue);
    }

    private void assertEquals(Object expected, Object actual) {
        if (expected instanceof File && actual instanceof File) {
            FileAssert.assertEquals((File) expected, (File) actual);
        } else {
            Assert.assertEquals(expected, actual);
        }
    }

}
