package com.microsoft.peersense.core;

import com.microsoft.peersense.core.exception.InvalidReadException;
import com.microsoft.peersense.support.CommonUtils;
import com.microsoft.peersense.support.Tests;
import com.microsoft.peersense.support.stream.PercentageProgressListener;
import com.microsoft.peersense.support.stream.Stream;
import junitx.framework.FileAssert;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 9/23/16.
 */
public class PayloadTest {

    private static final Logger log = LoggerFactory.getLogger(PayloadTest.class);

    private PayloadReader reader;
    private Payload payload;

    @Before
    public void beforeEachTest() {
        (new File(Tests.TEMP_DIR)).mkdirs();
    }

    @Test
    public void shouldCreatePrivatePacket() {
        payload = PayloadBuilder.newPrivatePayload();
        Assert.assertTrue(payload.isPrivate());
    }

    @Test
    public void shouldBeAbleToCheckPrivacyRepeatedly() {
        payload = PayloadBuilder.newPrivatePayload();
        Assert.assertTrue(payload.isPrivate() && payload.isPrivate() && payload.isPrivate());

        payload = PayloadBuilder.newPayload();
        Assert.assertTrue(!payload.isPrivate() && !payload.isPrivate() && !payload.isPrivate());
    }

    @Test
    public void shouldCreatePublicPacket() {
        payload = PayloadBuilder.newPayload();
        Assert.assertFalse(payload.isPrivate());
    }

    @Test
    public void shouldReadSameFileThatWasWritten() throws IOException {
        payload = PayloadBuilder.newPayload(Tests.FILE);
        reader = PayloadReader.forSource(payload);
        //String targetFilePath = Tests.TEMP_DIR + "/1.txt";
        File readFile = (File) reader.read(File.class);
        FileAssert.assertEquals(Tests.FILE, readFile);
        Assert.assertEquals(0, Stream.available(payload.getContentStream()));
    }

    @Test
    public void shouldReadSameStringThatWasWritten() {
        payload = PayloadBuilder.newPayload(Tests.STRING);
        reader = PayloadReader.forSource(payload);
        Assert.assertEquals(Tests.STRING, reader.read(String.class));
        Assert.assertEquals(0, Stream.available(payload.getContentStream()));
    }

    @Test
    public void shouldReadSameObjectThatWasWritten() {
        payload = PayloadBuilder.newPayload(Tests.OBJECT);
        reader = PayloadReader.forSource(payload);
        Tests.TestObject actual = (Tests.TestObject) reader.read(Tests.TestObject.class);
        Assert.assertEquals(Tests.OBJECT.field1, actual.field1);
        Assert.assertEquals(Tests.OBJECT.field2, actual.field2);
        Assert.assertEquals(0, Stream.available(payload.getContentStream()));
    }

    @Test
    public void shouldReadSequenceOfElementsInCorrectOrder() {
        Object[] payloadItems = {
                Tests.INTEGER,
                Tests.FILE,
                Tests.STRING,
                Tests.OBJECT,
                Tests.LONG
        };

        payload = PayloadBuilder.newPayload(payloadItems);
        reader = PayloadReader.forSource(payload);

        Object[] readItems = reader.read(CommonUtils.getTypeArray(payloadItems));

        Assert.assertEquals(readItems.length, payloadItems.length);
        for (int i = 0; i < readItems.length; i++)
            assertEqualItems(payloadItems[i], readItems[i]);

        Assert.assertEquals(0, Stream.available(payload.getContentStream()));
    }

    private void assertEqualItems(Object first, Object second) {
        if (first instanceof File && second instanceof File) {
            FileAssert.assertEquals((File) first, (File) second);
        } else {
            Assert.assertEquals(first, second);
        }
    }

    @Test(expected = InvalidReadException.class)
    public void shouldThrowIfFileReadIsAttemptedOnAStreamContainingObject() {
        payload = PayloadBuilder.newPayload(Tests.STRING);
        reader = PayloadReader.forSource(payload);

        //String targetFilePath = Tests.TEMP_DIR + "/2.txt";
        reader.read(File.class);
    }

    @Test(expected = InvalidReadException.class)
    public void shouldThrowIfObjectReadIsAttemptedOnAStreamContainingFile() {
        payload = PayloadBuilder.newPayload(Tests.FILE);
        reader = PayloadReader.forSource(payload);
        reader.read(Integer.class);
    }

    @Test
    public void creationOfMultipleReadersShouldNotChangePrivateStreamState() {
        Payload payload1 = PayloadBuilder.newPrivatePayload(PayloadState.PRIVATE);
        Payload payload2 = PayloadBuilder.newPrivatePayload(PayloadState.PRIVATE);

        // Two readers were created for stream1
        PayloadReader.forSource(payload1);
        PayloadReader.forSource(payload1);

        // Only one reader was created for stream2
        PayloadReader.forSource(payload2);

        Assert.assertEquals(payload1, payload2);
    }

    @Test(expected = ClassCastException.class)
    public void shouldThrowWhenReadingAnyUnwrittenObject() {
        payload = PayloadBuilder.newPayload(Tests.STRING);
        reader = PayloadReader.forSource(payload);
        Integer integer = (Integer) reader.read(String.class);
    }

    @Test
    public void shouldWorkCorrectlyForStreamContainingMinusOne() {
        byte value = -1;
        payload = PayloadBuilder.newPayload(value, value, value);
        reader = PayloadReader.forSource(payload);
        Assert.assertTrue(value == (Byte) reader.read(Byte.class));
        Assert.assertTrue(value == (Byte) reader.read(Byte.class));
        Assert.assertTrue(value == (Byte) reader.read(Byte.class));
    }

    @Test
    public void shouldConvertToStringWithoutAffectingState() {
        payload = PayloadBuilder.newPayload(Tests.BYTE);
        String representation1 = payload.toString();
        String representation2 = payload.toString();
        Assert.assertEquals(representation1, representation2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIfComparedToOtherObjects() {
        payload = PayloadBuilder.newPayload();
        payload.equals(Tests.OBJECT);
    }

    @Test(expected = InvalidReadException.class)
    public void shouldThrowIfAMalformedStreamIsPassesToConstructor() {
        payload = new Payload(Stream.build(Tests.BYTE));
    }

    @Test
    public void sizeShouldBeNonZero() {
        payload = PayloadBuilder.newPayload(Tests.INTEGER);
        Assert.assertTrue(payload.getSize() > 0);
    }

    @Test
    public void sizeShouldBeProportionalToContent() {
        payload = PayloadBuilder.newPayload(Tests.STRING);
        Payload payload2 = PayloadBuilder.newPayload(Tests.STRING + " ");
        Assert.assertTrue(payload.getSize() + 1 == payload2.getSize());
    }

    @Test
    public void shouldReportProgressForSingle() {
        payload = PayloadBuilder.newPayload(Tests.LARGE_FILE, Tests.LARGE_STRING);
        reader = PayloadReader.forSource(payload);

        int expectedMinimumReports = 4;// at least 4 reports for large file
        final List<Float> reports = new ArrayList<>(expectedMinimumReports);
        reader.read(File.class, new PercentageProgressListener() {
            @Override
            public void onPercentageCompleted(float percentageCompleted) {
                reports.add(percentageCompleted);
            }
        });

        Tests.validatePercentageProgressReport(reports, expectedMinimumReports);
    }

    @Test
    public void shouldReportProgressForMultipleReads() {
        payload = PayloadBuilder.newPayload(Tests.LARGE_STRING, Tests.LARGE_FILE, Tests.LARGE_STRING, Tests.LARGE_FILE);
        reader = PayloadReader.forSource(payload);

        int expectedMinimumReports = 2 * 4; // at least 2 reports for each large object
        final List<Float> reports = new ArrayList<>(expectedMinimumReports);
        reader.read(new Type[]{String.class, File.class, String.class, File.class}, new PercentageProgressListener() {
            @Override
            public void onPercentageCompleted(float percentageCompleted) {
                reports.add(percentageCompleted);
            }
        });

        Tests.validatePercentageProgressReport(reports, expectedMinimumReports);
    }

    @Test
    public void printTypicalPayloadSizeMetrics() {
        log.info("------ METRICS ------");

        Payload shortObjectPayload = PayloadBuilder.newPayload(
                Tests.BYTE,
                Tests.SHORT,
                Tests.INTEGER,
                Tests.LONG,
                Tests.DOUBLE,
                Tests.FLOAT,
                Tests.STRING,
                Tests.FILE
        );
        log.info("   Short    " + shortObjectPayload.getSize());

        Payload largeObjectPayload = PayloadBuilder.newPayload(
                Tests.LARGE_FILE,
                Tests.LARGE_STRING
        );
        log.info("   Large    " + largeObjectPayload.getSize());
        log.info("---------------------");
    }

    @After
    public void afterEachTest() throws IOException {
        if (reader != null) reader.close();
    }

}
