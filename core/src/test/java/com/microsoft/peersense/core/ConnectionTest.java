package com.microsoft.peersense.core;

import com.microsoft.peersense.core.exception.NetworkException;
import com.microsoft.peersense.support.Tests;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;

/**
 * Created by shubham on 3/28/17.
 */
public class ConnectionTest {

    private Connection connection = Tests.CONNECTION;
    private Payload payload = PayloadBuilder.newPayload(Tests.BYTE);

    @Test
    public void shouldNotReportWhenNullListenerIsPassed() {
        connection.send(payload, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenNullPayloadIsPassed() {
        connection.send(null, null);
    }

    @Test(expected = NetworkException.class)
    public void shouldThrowIfSendFails() throws Exception {
        Connection spy = spy(new Tests.StubConnection());
        doThrow(new IOException()).when(spy).createNewSession();
        spy.send(payload);
    }

}
