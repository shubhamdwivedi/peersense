package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.core.exception.InvalidReadException;
import com.microsoft.peersense.support.stream.Stream;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by shubham on 4/19/17.
 */
public class UnexpectedProtocolTest {

    private static final int EXPECTED_CODE = 1;
    private static final int UNEXPECTED_CODE = -1;

    @Test(expected = InvalidReadException.class)
    public void shouldThrowIfUnexpectedTypePresent() {
        InputStream streamWithUnexpectedType = (new UnexpectedProtocol()).serialize("").getStream();
        (new ExpectedProtocol()).deserialize(streamWithUnexpectedType);
    }

    private abstract class StubProtocol extends FixedSizeProtocol<Object> {

        @Override
        protected int getFixedSize() {
            return 1;
        }

        @Override
        protected Object getDeserialized(InputStream stream) throws IOException {
            return (byte) 1;
        }

        @Override
        protected InputStream getSerializedStream(Object object) throws IOException {
            return Stream.build((byte) 1);
        }

    }

    private class UnexpectedProtocol extends StubProtocol {
        @Override
        protected byte getTypeCode() {
            return UNEXPECTED_CODE;
        }
    }

    private class ExpectedProtocol extends StubProtocol {
        @Override
        protected byte getTypeCode() {
            return EXPECTED_CODE;
        }
    }
}
