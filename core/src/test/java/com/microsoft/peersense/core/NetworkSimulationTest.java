package com.microsoft.peersense.core;

import com.microsoft.peersense.support.concurrency.Wait;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import static org.mockito.Mockito.spy;

/**
 * Created by shudwi on 2/20/2017.
 */
@Ignore("Handshake not tested properly. Needs to be corrected")
public class NetworkSimulationTest {

    private static Connection connection1, connection2, connection3;
    private static Module module;
    private static Network network, foreignNetworkInstance;
    private static boolean peer1Found = false, peer2Lost = false, peer3Lost = false, dataReceived = false;

    @BeforeClass
    public static void runSimulation() {
        connection1 = spy(new SConnection());
        connection2 = spy(new SConnection());
        connection3 = spy(new SConnection());
        module = new SModule();

        network = Network.getInstance();
        network.install(module);
        foreignNetworkInstance = new Network();

        network.addListener(new NetworkListener() {
            @Override
            protected void onDiscovered(Connection connection) {
                if (connection.getDeviceId().equals(connection1.getDeviceId())) {
                    peer1Found = true;
                }
            }

            @Override
            protected void onLost(Connection connection) {
                if (connection2.getDeviceId().equals(connection.getDeviceId())) {
                    peer2Lost = true;
                }
                if (connection3.getDeviceId().equals(connection.getDeviceId())) {
                    peer3Lost = true;
                }
            }

            @Override
            protected void onReceived(Connection connection, Payload payload) {
                dataReceived = (connection.getDeviceId().equals(connection1.getDeviceId())) && !payload.isPrivate();
            }
        });

        network.startup();
        // Let the network run for a while
        Wait.period(500);
        network.shutdown();
    }

    // A short pause of between 10 and 20 milliseconds
    private static void hiccup() {
        Wait.period(10 + (int) (Math.random() * 10));
    }

    @Test
    public void testDiscoveryHandshake() {
        Assert.assertTrue(peer1Found);
    }

    @Test
    public void peerShouldBeMarkedLostIfOnLostIsInvoked() {
        Assert.assertTrue(peer2Lost);
    }

    @Test
    public void peerShouldBeMarkedLostIfIQuitPacketIsReceived() {
        Assert.assertTrue(peer3Lost);
    }

    @Test
    public void shouldReceivePublicDataInRegisteredListener() {
        Assert.assertTrue(dataReceived);
    }

    private static class SConnection extends Connection {

        SConnection() {
            setDeviceId(UUID.randomUUID().toString());
        }

        @Override
        public OutputSession createNewSession() {
            return new OutputSession() {
                @Override
                protected OutputStream getOutputStream() throws IOException {
                    return new ByteArrayOutputStream();
                }
            };
        }
    }

    private static class SModule extends Module {
        @Override
        public void startup() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    hiccup();
                    // Test discovery handshake - Module reports a connection has been discovered. The metadata request
                    // is sent after this. (Which is still untested by this test suite. A unit test that tests that
                    // the request is sent right after discovery is needed.)
                    onDiscovered(connection1);

                    hiccup();
                    // Simulate the scenario where the discovered connection responds with the metadata properly
                    onDataReceived(connection1, Message.metadata(foreignNetworkInstance).getStream());
                    hiccup();

                    // Test when a metadata response is received
                    onDataReceived(connection1, Message.metadata(network).getStream());
                    hiccup();

                    // Test onLost invocation
                    onLost(connection2);
                    hiccup();

                    // Multiple onLost invocations should not cause any issue
                    onLost(connection2);
                    onLost(connection2);
                    hiccup();

                    // Should pass on public data to installed listeners
                    onDataReceived(connection1, PayloadBuilder.newPayload().getStream());
                    hiccup();

                    // Test when a peer sends I_QUIT
                    onDataReceived(connection3, Message.iQuit().getStream());
                    hiccup();

                    // Should ignore when a peer sends malformed private data
                    onDataReceived(connection1, PayloadBuilder.newPrivatePayload(Message.METADATA)/*More data expected here*/.getStream());
                    hiccup();

                    // Should ignore when a peer sends unrecognized private data
                    onDataReceived(connection1, PayloadBuilder.newPrivatePayload(1234)/*More data expected here*/.getStream());
                }
            }).start();
        }

        @Override
        public void shutdown() {
        }
    }

}
