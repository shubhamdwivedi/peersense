package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.Tests;
import com.microsoft.peersense.support.stream.PercentageProgressListener;
import lombok.AllArgsConstructor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by shubham on 5/7/17.
 */
@RunWith(Parameterized.class)
@AllArgsConstructor
public class DeserializationProgressTest {

    private Protocol protocol;
    private Object testValue;
    private int expectedMinimumReports;

    @Parameterized.Parameters(name = "test: {0}")
    public static Collection<Object[]> data() {
        int expectedReportsForLargeObject = Tests.LARGE_STRING.length() / SerializableProtocol.CHUNK;
        int expectedReportsForLargeFile = (int) (Tests.LARGE_FILE.length() / FileProtocol.CHUNK);

        return Arrays.asList(new Object[][]{
                {new ByteProtocol(), Tests.BYTE, 1},
                {new IntegerProtocol(), Tests.INTEGER, 1},
                {new LongProtocol(), Tests.LONG, 1},
                {new SerializableProtocol(), Tests.LARGE_STRING, expectedReportsForLargeObject},
                {new SerializableProtocol(), Tests.LARGE_OBJECT, expectedReportsForLargeObject},
                {new FileProtocol(), Tests.LARGE_FILE, expectedReportsForLargeFile},
        });
    }

    @Test
    public void shouldReportProgress() {
        final List<Float> reports = new ArrayList<>(expectedMinimumReports);
        InputStream stream = protocol.serialize(testValue).getStream();
        protocol.deserialize(stream, new PercentageProgressListener() {
            @Override
            public void onPercentageCompleted(float percentageCompleted) {
                reports.add(percentageCompleted);
            }
        });
        Tests.validatePercentageProgressReport(reports, expectedMinimumReports);
    }

}

