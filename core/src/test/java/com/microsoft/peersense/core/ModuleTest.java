package com.microsoft.peersense.core;

import com.microsoft.peersense.support.Tests;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by shubham on 2/21/17.
 */
public class ModuleTest {

    private static InputStream stream = PayloadBuilder.newPayload().getStream();
    private static Connection testConnection = Tests.CONNECTION;
    private static boolean discoveredCallbackCalled = false;
    private static boolean lostCallbackCalled = false;
    private static boolean dataReceivedCallbackCalled = false;

    @BeforeClass
    public static void beforeAllTests() throws IOException {

        Module module = new Module() {
            @Override
            public void startup() {
                onDiscovered(testConnection);
                onLost(testConnection);
                onDataReceived(Tests.CONNECTION, stream);
            }

            @Override
            public void shutdown() {
            }
        };

        module.addListener(new NetworkListener() {
            @Override
            protected void onDiscovered(Connection connection) {
                Assert.assertEquals(testConnection, connection);
                discoveredCallbackCalled = true;
            }

            @Override
            protected void onLost(Connection connection) {
                Assert.assertEquals(testConnection, connection);
                lostCallbackCalled = true;
            }

            @Override
            protected void onReceived(Connection sender, Payload payload) {
                Assert.assertEquals(Tests.CONNECTION, sender);
                dataReceivedCallbackCalled = true;
            }
        });

        module.startup();
    }

    @Test
    public void testDiscoveredCallback() {
        Assert.assertTrue(discoveredCallbackCalled);
    }

    @Test
    public void testLostCallback() {
        Assert.assertTrue(lostCallbackCalled);
    }

    @Test
    public void testDataReceivedCallback() {
        Assert.assertTrue(dataReceivedCallbackCalled);
    }

}
