package com.microsoft.peersense.core;

import com.microsoft.peersense.support.MyAssert;
import com.microsoft.peersense.support.Tests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by shubham on 10/3/16.
 */
public class NetworkTest {

    private Network network;
    private Module module;

    @Before
    public void beforeEachTest() {
        network = new Network();
        module = new Tests.StubModule();
    }

    @Test
    public void testNullInstall() {
        network.install(null);
        Assert.assertEquals(0, network.getInstalledModules().size());
    }

    @Test
    public void testDuplicateInstall() {
        network.install(module);
        network.install(module);
        Assert.assertEquals(1, network.getInstalledModules().size());
    }

    @Test
    public void shouldNotBeAbleToSetNullNetworkId() {
        network.setNetworkId(null);
        Assert.assertNotNull(network.getNetworkId());
    }

    @Test
    public void shouldNotBeAbleToSetBlankNetworkId() {
        network.setNetworkId("");
        Assert.assertNotEquals(0, network.getNetworkId().length());
    }

    @Test
    public void shouldBeASingleton() {
        MyAssert.assertAllEqual(Network.getInstance(), Network.getInstance(), Network.getInstance());
    }

    @Test
    public void shouldSetNetworkId() {
        network.setNetworkId(Tests.STRING);
        Assert.assertEquals(Tests.STRING, network.getNetworkId());
    }

    @Test
    public void deviceIdShouldNotBeBlank() {
        Assert.assertNotNull(network.getHostDeviceId());
        Assert.assertNotEquals(0, network.getHostDeviceId().length());
    }

    @Test
    public void defaultNetworkIdShouldBeSameForDifferentClients() throws Exception {
        Network network2 = new Network();
        Assert.assertEquals(network.getNetworkId(), network2.getNetworkId());
    }

    @Test
    public void defaultDeviceIdShouldBeDifferentForDifferentClients() throws Exception {
        Network network2 = new Network();
        Assert.assertNotEquals(network.getHostDeviceId(), network2.getHostDeviceId());
    }

}
