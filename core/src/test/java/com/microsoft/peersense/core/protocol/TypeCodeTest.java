package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.MyAssert;
import org.junit.Test;

/**
 * Created by shubham on 3/28/17.
 */
public class TypeCodeTest {

    @Test
    public void allTypeCodesForAllProtocolsMustBeDifferent() {
        MyAssert.assertAllDifferent(
                new ByteProtocol().getTypeCode(),
                new ShortProtocol().getTypeCode(),
                new IntegerProtocol().getTypeCode(),
                new LongProtocol().getTypeCode(),
                new FileProtocol().getTypeCode(),
                new SerializableProtocol().getTypeCode());
    }

}
