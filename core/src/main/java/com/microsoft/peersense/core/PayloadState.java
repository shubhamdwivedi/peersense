package com.microsoft.peersense.core;

import com.microsoft.peersense.core.protocol.ByteProtocol;
import com.microsoft.peersense.core.protocol.LongProtocol;
import com.microsoft.peersense.core.protocol.Protocol;
import com.microsoft.peersense.support.stream.ProgressListener;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by shubham on 3/28/17.
 */
@AllArgsConstructor
class PayloadState {

    // These bytes mark the visibility of the payload. A private payload
    // is processed by the PeerSense library  internally while a  public
    // payload is forwarded to external listeners.
    static final byte PRIVATE = 1;
    static final byte PUBLIC = 2;

    @Getter
    private boolean isPrivate;

    @Getter
    private long size;

    static class StateProtocol extends Protocol<PayloadState> {

        private static final ByteProtocol byteProtocol = new ByteProtocol();
        private static final LongProtocol longProtocol = new LongProtocol();

        @Override
        protected DeserializationResult getDeserialized(InputStream stream, ProgressListener listener) throws IOException {
            DeserializationResult dHead = byteProtocol.deserialize(stream);
            DeserializationResult dSize = longProtocol.deserialize(stream);
            PayloadState state = new PayloadState(((byte) dHead.getObject()) == PRIVATE, (long) dSize.getObject());
            return build(state, dHead.getSize() + dSize.getSize());
        }

        @Override
        protected SerializationResult getSerialized(PayloadState payloadState) {
            SerializationResult head = byteProtocol.serialize(payloadState.isPrivate() ? PRIVATE : PUBLIC);
            SerializationResult size = longProtocol.serialize(payloadState.getSize());
            return combine(head, size);
        }

        @Override
        protected byte getTypeCode() {
            return 10;
        }

    }

}
