package com.microsoft.peersense.core;

import com.microsoft.peersense.core.protocol.LongProtocol;
import com.microsoft.peersense.core.protocol.Protocol;
import com.microsoft.peersense.support.stream.Stream;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by shubham on 9/23/16.
 */
public class PayloadBuilder {

    private static final PayloadState.StateProtocol stateProtocol = new PayloadState.StateProtocol();
    private static final LongProtocol longProtocol = new LongProtocol();

    private boolean isPrivate;
    private long sizeInBytes;
    private List<InputStream> streams;

    private PayloadBuilder(boolean isPrivate, Object... payloadItems) {
        this.isPrivate = isPrivate;
        this.sizeInBytes = 0;
        streams = new LinkedList<>();

        for (Object item : payloadItems)
            put(item);
    }

    public static Payload newPayload(Object... payloadItems) {
        return new PayloadBuilder(false, payloadItems).build();
    }

    static Payload newPrivatePayload(Object... payloadItems) {
        return new PayloadBuilder(true, payloadItems).build();
    }

    private void put(Object object) {
        Protocol.SerializationResult result = Payload.getProtocolFor(object).serialize(object);
        streams.add(result.getStream());
        sizeInBytes += result.getSize();
    }

    private Payload build() {
        PayloadState state = new PayloadState(isPrivate, sizeInBytes);
        streams.add(0, stateProtocol.serialize(state).getStream());
        return new Payload(Stream.attach(streams));
    }

}
