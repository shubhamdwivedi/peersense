package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.CommonUtils;
import com.microsoft.peersense.support.stream.ProgressListener;
import com.microsoft.peersense.support.stream.Stream;

import java.io.*;

/**
 * Created by shubham on 3/3/17.
 */
public class FileProtocol extends Protocol<File> {

    static final int CHUNK = 512 * 1024;
    private static final SerializableProtocol serializableProtocol = new SerializableProtocol();
    private static final LongProtocol longProtocol = new LongProtocol();
    private String destinationDir;

    public FileProtocol(String destinationDir) {
        this.destinationDir = destinationDir;
    }

    public FileProtocol() {
        this(CommonUtils.tempDir("deserialized_files"));
    }

    @Override
    protected SerializationResult getSerialized(File file) throws IOException {

        SerializationResult sSize = longProtocol.serialize(file.length());
        SerializationResult sName = serializableProtocol.serialize(file.getName());
        SerializationResult sFile = build(new FileInputStream(file), file.length());
        return combine(sSize, sName, sFile);

    }

    @Override
    protected DeserializationResult getDeserialized(InputStream input, ProgressListener listener) throws IOException {

        long totalBytesAvailable = (long) longProtocol.deserialize(input).getObject();
        String fileName = (String) serializableProtocol.deserialize(input).getObject();
        File targetFile = new File(destinationDir + File.separator + fileName);
        FileOutputStream output = new FileOutputStream(targetFile);
        Stream.copy(input, output, totalBytesAvailable, CHUNK, listener);
        output.close();
        return build(targetFile, totalBytesAvailable);

    }

    @Override
    protected byte getTypeCode() {
        return Type.FILE.getCode();
    }

}
