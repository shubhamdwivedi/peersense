package com.microsoft.peersense.core;

/**
 * Created by shubham on 4/19/17.
 */
class Message {

    static final int METADATA_REQUEST = 1;
    static final int METADATA = 2;
    static final int I_QUIT = 3;
    static final int MONITOR = 4;

    static Payload metadataRequest() {
        return packet(METADATA_REQUEST);
    }

    static Payload metadata(Network network) {
        return PayloadBuilder.newPrivatePayload(METADATA, network.getNetworkId(), network.getHostDeviceId());
    }

    static Payload iQuit() {
        return packet(I_QUIT);
    }

    static Payload monitor() {
        return packet(MONITOR);
    }

    static private Payload packet(int code) {
        return PayloadBuilder.newPrivatePayload(code);
    }

}
