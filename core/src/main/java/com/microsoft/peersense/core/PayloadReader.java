package com.microsoft.peersense.core;

import com.microsoft.peersense.core.protocol.Protocol;
import com.microsoft.peersense.support.stream.PercentageProgressListener;
import com.microsoft.peersense.support.stream.ProgressListener;
import com.microsoft.peersense.support.stream.Stream;

import java.io.InputStream;
import java.lang.reflect.Type;

/**
 * Created by shubham on 9/23/16.
 */
public class PayloadReader {

    private InputStream stream;
    private long totalSize;
    private long totalBytesRead;

    private PayloadReader(Payload source) {
        this.stream = source.getContentStream();
        this.totalSize = source.getSize();
        this.totalBytesRead = 0;
    }

    public static PayloadReader forSource(Payload payload) {
        return new PayloadReader(payload);
    }

    public Object read(Type type, PercentageProgressListener listener) {
        return Payload.getProtocolFor(type).deserialize(stream, listener).getObject();
    }

    public Object read(Type type) {
        return read(type, PercentageProgressListener.NO_PROGRESS_REPORTING);
    }

    public Object[] read(Type[] types, final PercentageProgressListener listener) {
        Object[] objects = new Object[types.length];

        for (int i = 0; i < types.length; i++) {

            Protocol.DeserializationResult result = Payload.getProtocolFor(types[i]).deserialize(stream, new ProgressListener() {
                @Override
                public void onProgress(long bytesRead, long total) {
                    listener.onProgress(totalBytesRead + bytesRead, totalSize);
                }
            });

            objects[i] = result.getObject();
            totalBytesRead += result.getSize();

        }

        listener.onPercentageCompleted(100f);
        return objects;
    }

    public Object[] read(Type[] types) {
        return read(types, PercentageProgressListener.NO_PROGRESS_REPORTING);
    }


    public void close() {
        Stream.close(stream);
    }

}
