package com.microsoft.peersense.core.exception;

/**
 * Created by shubham on 2/21/17.
 */
public class NetworkException extends RuntimeException {

    public NetworkException(String message, Throwable cause) {
        super(message, cause);
    }

    public NetworkException(String message) {
        super(message);
    }

}
