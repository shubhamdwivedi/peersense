package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.stream.Stream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Created by shubham on 5/8/17.
 */
public class ShortProtocol extends FixedSizeProtocol<Short> {

    @Override
    protected int getFixedSize() {
        return Short.SIZE / 8;
    }

    @Override
    protected Short getDeserialized(InputStream stream) throws IOException {
        return ByteBuffer.wrap(Stream.read(stream, getFixedSize())).getShort();
    }

    @Override
    protected InputStream getSerializedStream(Short shortInteger) throws IOException {
        return Stream.build(ByteBuffer.allocate(getFixedSize()).putShort(shortInteger).array());
    }

    @Override
    protected byte getTypeCode() {
        return Type.SHORT.getCode();
    }

}
