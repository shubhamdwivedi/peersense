package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.stream.Stream;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by shubham on 3/3/17.
 */
public class ByteProtocol extends FixedSizeProtocol<Byte> {

    @Override
    protected InputStream getSerializedStream(Byte byteValue) throws IOException {
        return Stream.build(byteValue);
    }

    @Override
    protected Byte getDeserialized(InputStream stream) throws IOException {
        return (byte) stream.read();
    }

    @Override
    protected int getFixedSize() {
        return 1;
    }

    @Override
    protected byte getTypeCode() {
        return Type.BYTE.getCode();
    }

}
