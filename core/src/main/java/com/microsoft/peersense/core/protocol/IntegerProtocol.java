package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.stream.Stream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Created by shubham on 3/3/17.
 */
public class IntegerProtocol extends FixedSizeProtocol<Integer> {

    @Override
    protected InputStream getSerializedStream(Integer integer) throws IOException {
        return Stream.build(ByteBuffer.allocate(getFixedSize()).putInt(integer).array());
    }

    @Override
    protected Integer getDeserialized(InputStream stream) throws IOException {
        return ByteBuffer.wrap(Stream.read(stream, getFixedSize())).getInt();
    }

    @Override
    protected int getFixedSize() {
        return Integer.SIZE / 8;
    }

    @Override
    protected byte getTypeCode() {
        return Type.INTEGER.getCode();
    }

}
