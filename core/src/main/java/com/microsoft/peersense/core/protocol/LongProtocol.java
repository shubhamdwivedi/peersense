package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.stream.Stream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Created by shubham on 3/3/17.
 */
public class LongProtocol extends FixedSizeProtocol<Long> {

    @Override
    protected int getFixedSize() {
        return Long.SIZE / 8;
    }

    @Override
    protected Long getDeserialized(InputStream stream) throws IOException {
        return ByteBuffer.wrap(Stream.read(stream, getFixedSize())).getLong();
    }

    @Override
    protected InputStream getSerializedStream(Long longInteger) throws IOException {
        return Stream.build(ByteBuffer.allocate(getFixedSize()).putLong(longInteger).array());
    }

    @Override
    protected byte getTypeCode() {
        return Type.LONG.getCode();
    }

}
