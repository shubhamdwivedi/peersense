package com.microsoft.peersense.core;

import com.microsoft.peersense.support.CommonUtils;
import com.microsoft.peersense.support.concurrency.Condition;
import com.microsoft.peersense.support.concurrency.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shubham on 3/29/17.
 */
public class FunctionalTest {

    private static final Logger log = LoggerFactory.getLogger(FunctionalTest.class);
    private static final long TEST_RUN_TIME = 5 * 60 * 1000;

    private Map<Connection, Boolean> payloadDeliveryStatusMap;
    private Network network;

    private Object[] payloadItems = {
            (byte) 123,
            123,
            1234567890L,
            CommonUtils.createTemporaryFile(10), /*Small file*/
            CommonUtils.createTemporaryFile(10000), /*Large file*/
            "SerializableString",
    };

    public FunctionalTest() {
        this.payloadDeliveryStatusMap = new HashMap<>();
        this.network = Network.getInstance();

        network.addListener(new NetworkListener() {

            @Override
            protected void onDiscovered(final Connection connection) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runTests(connection);
                    }
                }).start();
            }

            @Override
            protected void onLost(Connection connection) {
            }

            @Override
            protected void onReceived(Connection sender, Payload payload) {
                FunctionalTest.this.onReceived(sender, payload);
            }

        });
    }

    private void onReceived(Connection connection, Payload payload) {
        Object[] receivedPayloadItems = PayloadReader.forSource(payload).read(CommonUtils.getTypeArray(payloadItems));
        if (payloadItems.length == receivedPayloadItems.length) {

            boolean areAllItemsReceived = true;
            for (int i = 0; i < payloadItems.length; i++) {
                if (!equals(payloadItems[i], receivedPayloadItems[i]))
                    areAllItemsReceived = false;
            }

            if (areAllItemsReceived)
                payloadDeliveryStatusMap.put(connection, true);

        }
    }

    public void run() {
        network.startup();
        Wait.period(TEST_RUN_TIME);
        network.shutdown();
    }

    private void runTests(final Connection connection) {
        connection.send(PayloadBuilder.newPayload(payloadItems));
        Wait.until(new Condition("completePayloadIsReceivedFromDiscoveredConnection") {
            @Override
            public boolean isSatisfied() {
                Boolean status = payloadDeliveryStatusMap.get(connection);
                return status != null && status;
            }
        });
        log.info("[SUCCESS] All tests ran successfully on " + connection);
    }

    private boolean equals(Object first, Object second) {
        if (first instanceof File && second instanceof File) {
            return ((File) first).length() == ((File) second).length();
        } else {
            return first.equals(second);
        }
    }

}
