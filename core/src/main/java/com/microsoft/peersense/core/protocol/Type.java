package com.microsoft.peersense.core.protocol;

import lombok.AllArgsConstructor;

/**
 * Created by shubham on 5/8/17.
 */

@AllArgsConstructor
public enum Type {

    BYTE(1),
    SHORT(2),
    INTEGER(3),
    LONG(4),
    FILE(5),
    SERIALIZABLE(6);

    private int code;

    byte getCode() {
        return (byte) code;
    }

}
