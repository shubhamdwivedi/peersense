package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.stream.ProgressListener;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by shubham on 5/8/17.
 */
public abstract class FixedSizeProtocol<T> extends Protocol<T> {

    @Override
    protected final SerializationResult getSerialized(T object) throws IOException {
        return build(getSerializedStream(object), (long) getFixedSize());
    }

    @Override
    protected final DeserializationResult getDeserialized(InputStream stream, ProgressListener listener) throws IOException {
        T deserializedObject = getDeserialized(stream);
        listener.onProgress(getFixedSize(), getFixedSize());
        return build(deserializedObject, (long) getFixedSize());
    }

    protected abstract int getFixedSize();

    protected abstract T getDeserialized(InputStream stream) throws IOException;

    protected abstract InputStream getSerializedStream(T object) throws IOException;

}
