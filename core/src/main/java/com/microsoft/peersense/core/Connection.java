package com.microsoft.peersense.core;

import com.microsoft.peersense.core.exception.NetworkException;
import com.microsoft.peersense.support.stream.PercentageProgressListener;
import com.microsoft.peersense.support.stream.Stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by shubham on 9/24/16.
 */
public abstract class Connection {

    private String deviceId;

    public final void send(Payload payload) {
        send(payload, PercentageProgressListener.NO_PROGRESS_REPORTING);
    }

    public final void send(Payload payload, PercentageProgressListener progressListener) {
        if (payload == null)
            throw new IllegalArgumentException("Payload cannot be null");

        if (progressListener == null)
            progressListener = PercentageProgressListener.NO_PROGRESS_REPORTING;

        try {

            OutputSession session = createNewSession();
            session.onStart();

            InputStream input = payload.getStream();
            OutputStream output = session.getOutputStream();
            Stream.copy(input, output, payload.getSize(), progressListener);
            input.close();

            session.onComplete();

        } catch (IOException e) {
            throw new NetworkException("Failed to send to " + this, e);
        }

    }

    protected abstract OutputSession createNewSession() throws IOException;

    public final String getDeviceId() {
        return deviceId;
    }

    final void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    public abstract class OutputSession {

        protected void onStart() throws IOException {
        }

        protected abstract OutputStream getOutputStream() throws IOException;

        protected void onComplete() throws IOException {
        }

    }

}
