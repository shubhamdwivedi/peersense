package com.microsoft.peersense.core.exception;

/**
 * Created by shubham on 9/23/16.
 */
public class InvalidReadException extends RuntimeException {

    public InvalidReadException(String message) {
        super(message);
    }

}
