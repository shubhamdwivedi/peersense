package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.core.exception.InvalidReadException;
import com.microsoft.peersense.core.exception.RuntimeIOException;
import com.microsoft.peersense.support.stream.PercentageProgressListener;
import com.microsoft.peersense.support.stream.ProgressListener;
import com.microsoft.peersense.support.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by shubham on 3/3/17.
 */
public abstract class Protocol<T> {

    private static final Logger log = LoggerFactory.getLogger(Protocol.class);

    // Return object in a typed serialized stream along with stream size
    public SerializationResult serialize(T object) {
        try {

            SerializationResult result = getSerialized(object);
            InputStream streamWithTypeCode = Stream.attach(Stream.build(getTypeCode()), result.getStream());
            return build(streamWithTypeCode, result.getSize() + 1);

        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    public DeserializationResult deserialize(InputStream stream) {
        return deserialize(stream, PercentageProgressListener.NO_PROGRESS_REPORTING);
    }

    public DeserializationResult deserialize(InputStream stream, ProgressListener listener) {
        try {
            byte actualCode = (byte) stream.read();
            byte expectedCode = getTypeCode();

            if (actualCode != expectedCode)
                throw new InvalidReadException("Unexpected item found in stream! Expected code [" + expectedCode + "], but found [" + actualCode + "]");

            DeserializationResult result = getDeserialized(stream, listener);
            return build(result.getObject(), result.getSize() + 1);

        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    protected abstract SerializationResult getSerialized(T object) throws IOException;

    protected abstract DeserializationResult getDeserialized(InputStream stream, ProgressListener listener) throws IOException;

    protected abstract byte getTypeCode();

    // Helper methods to build and combine serialization/deserialization results
    protected SerializationResult build(InputStream stream, Long size) {
        return new SerializationResult(stream, size);
    }

    protected DeserializationResult build(Object object, Long size) {
        return new DeserializationResult(object, size);
    }

    protected SerializationResult combine(SerializationResult... results) {
        List<InputStream> streams = new LinkedList<>();
        long totalSize = 0;
        for (SerializationResult result : results) {
            streams.add(result.getStream());
            totalSize += result.getSize();
        }
        return new SerializationResult(Stream.attach(streams), totalSize);
    }

    @AllArgsConstructor
    public static final class SerializationResult {
        @Getter
        private InputStream stream;
        @Getter
        private long size;
    }

    @AllArgsConstructor
    public static final class DeserializationResult {
        @Getter
        private Object object;
        @Getter
        private long size;
    }

}
