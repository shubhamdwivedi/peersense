package com.microsoft.peersense.core;

import com.microsoft.peersense.core.exception.RuntimeIOException;
import com.microsoft.peersense.core.protocol.*;
import com.microsoft.peersense.support.stream.Stream;

import java.io.*;

/**
 * Created by shubham on 9/24/16.
 */
public class Payload {

    private static final int PUSHBACK_BUFFER_BYTES = 4096;
    private static final PayloadState.StateProtocol stateProtocol = new PayloadState.StateProtocol();

    private PayloadState state;
    private PushbackInputStream contentStream;
    private Protocol.SerializationResult serialisedState;

    Payload(InputStream sourceStream) {
        state = (PayloadState) stateProtocol.deserialize(sourceStream).getObject();
        serialisedState = stateProtocol.serialize(state);
        contentStream = new PushbackInputStream(sourceStream, PUSHBACK_BUFFER_BYTES);
    }

    static Protocol getProtocolFor(Object object) {
        if (object instanceof Byte || object.equals(Byte.class))
            return new ByteProtocol();
        if (object instanceof Short || object.equals(Short.class))
            return new ShortProtocol();
        else if (object instanceof Integer || object.equals(Integer.class))
            return new IntegerProtocol();
        else if (object instanceof Long || object.equals(Long.class))
            return new LongProtocol();
        else if (object instanceof File || object.equals(File.class))
            return new FileProtocol();
        else
            return new SerializableProtocol();
    }

    public static byte[] peek(PushbackInputStream inputStream, int numberOfBytes) {
        try {
            byte[] contents = new byte[numberOfBytes];
            inputStream.read(contents);
            inputStream.unread(contents);
            return contents;
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    public long getSize() {
        return serialisedState.getSize() + state.getSize();
    }

    public boolean isPrivate() {
        return state.isPrivate();
    }

    // This is used by modules to transfer the payload stream to other clients.
    // This stream contains all the information to  rebuild the payload object
    // at the other end.
    InputStream getStream() {
        return Stream.attach(serialisedState.getStream(), contentStream);
    }

    // This is used by PayloadReader to the read the contents. Note that this
    // stream contains only the contents and no other information.
    InputStream getContentStream() {
        return new BufferedInputStream(contentStream);
    }

    // Following methods are overridden only for facilitating unit tests. There
    // are scenarios where two short streams need to be compared or printed
    // without affecting state.
    @Override
    public boolean equals(Object otherStream) {
        if (!(otherStream instanceof Payload))
            throw new IllegalArgumentException("Object is not a Payload");

        byte[] myContents = peek(contentStream, PUSHBACK_BUFFER_BYTES);
        byte[] otherContents = peek(((Payload) otherStream).contentStream, PUSHBACK_BUFFER_BYTES);

        boolean isContentSame = true;
        for (int i = 0; i < PUSHBACK_BUFFER_BYTES; i++) {
            isContentSame = isContentSame && (myContents[i] == otherContents[i]);
        }

        return isContentSame;
    }

}
