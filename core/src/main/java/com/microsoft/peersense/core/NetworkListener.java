package com.microsoft.peersense.core;

import com.microsoft.peersense.support.callback.Listener;

/**
 * Created by shubham on 10/8/16.
 */
public abstract class NetworkListener implements Listener {

    @Override
    public final void onUpdated(Object... arguments) {
        switch ((int) arguments[0]) {

            case Event.DATA_RECEIVED:
                onReceived((Connection) arguments[1], (Payload) arguments[2]);
                break;

            case Event.CONNECTION_DISCOVERED:
                onDiscovered((Connection) arguments[1]);
                break;

            case Event.CONNECTION_LOST:
                onLost((Connection) arguments[1]);
                break;

        }
    }

    protected abstract void onDiscovered(Connection connection);

    protected abstract void onLost(Connection connection);

    protected abstract void onReceived(Connection connection, Payload payload);

}
