package com.microsoft.peersense.core;

import com.google.common.collect.Sets;
import com.microsoft.peersense.core.exception.InvalidReadException;
import com.microsoft.peersense.core.exception.NetworkException;
import com.microsoft.peersense.support.callback.Listenable;
import com.microsoft.peersense.support.concurrency.Wait;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by shubham on 9/24/16.
 */
public class Network extends Listenable<NetworkListener> {

    private static final Logger log = LoggerFactory.getLogger(Network.class);
    private static Network instance;

    @Getter
    private String hostDeviceId;
    @Getter
    private String networkId;
    @Getter
    private Set<Module> installedModules;

    private Set<Connection> monitoredConnections;
    private ConnectionMonitorThread connectionMonitorThread;

    // This constructor is made  visible to unit tests only. Multiple unit tests require
    // different  configurations of the network  instance. Using this  constructor every
    // constructor can create a fresh network instance and hence can be independent from
    // other unit tests.
    Network() {

        // The device Id is a random id which is set when Network.class is loaded
        // into memory. The primary purpose of this id is to identify a particular
        // device uniquely. Note that, two peers can point to the same device, but
        // will have the same device id. On every  distinct device this class will
        // be  loaded at  least once and the  device id will be  shared by all the
        // installed modules. This is not modifiable.
        hostDeviceId = UUID.randomUUID().toString();

        // The default value of network id is same across all devices. This is done
        // to ensure that by default, if two clients don't set a private network id,
        // they  should be able to  connect to any other client who hasn't done the
        // same. If we set a random value here like device id, by default no client
        // will be able to discover any other client on network until and unless it
        // sets a private network id and  some other  client also  connects to that
        // same private network.
        networkId = "PeerSense";

        installedModules = new HashSet<>();
        monitoredConnections = Sets.newConcurrentHashSet();
        connectionMonitorThread = new ConnectionMonitorThread();

    }

    public static Network getInstance() {
        if (instance == null) {
            instance = new Network();
        }
        return instance;
    }

    public void setNetworkId(String networkId) {
        if (StringUtils.isNotBlank(networkId)) {
            this.networkId = networkId;
        }
    }

    public void install(Module module) {
        if (module == null) {
            return;
        }
        if (installedModules.contains(module)) {
            return;
        }

        module.addListener(new NetworkListener() {
            @Override
            protected void onDiscovered(Connection connection) {
                Network.this.onDiscovered(connection);
            }

            @Override
            protected void onLost(Connection connection) {
                Network.this.onLost(connection);
            }

            @Override
            protected void onReceived(Connection sender, Payload payload) {
                Network.this.onDataReceived(sender, payload);
            }
        });

        installedModules.add(module);
    }

    public void startup() {
        log.debug("Host: " + hostDeviceId);
        for (Module module : installedModules) {
            module.startup();
        }
        connectionMonitorThread.start();
    }

    public void shutdown() {
        for (Module module : installedModules) {
            module.shutdown();
            module.removeAllListeners();
        }
        for (Connection connection : monitoredConnections) {
            connection.send(Message.iQuit());
        }
        connectionMonitorThread.terminate();
        removeAllListeners();
    }

    private void onDiscovered(Connection connection) {
        log.debug("[METADATA_REQUEST] sent to " + connection);
        connection.send(Message.metadataRequest());
    }

    private void onLost(Connection connection) {
        monitoredConnections.remove(connection);
        Network.this.updateListeners(Event.CONNECTION_LOST, connection);
    }

    private void onDataReceived(Connection sender, Payload payload) {
        if (payload.isPrivate()) {
            onPrivateStreamReceived(sender, payload);
        } else {
            updateListeners(Event.DATA_RECEIVED, sender, payload);
        }
    }

    private void onPrivateStreamReceived(Connection connection, Payload payload) {
        PayloadReader reader = PayloadReader.forSource(payload);
        try {
            switch ((int) reader.read(Integer.class)) {

                case Message.METADATA_REQUEST:
                    log.debug("[METADATA_REQUEST] from " + connection);
                    connection.send(Message.metadata(this));
                    break;

                case Message.METADATA:
                    String networkId = (String) reader.read(String.class);
                    if (networkId.equals(getNetworkId())) {
                        connection.setDeviceId((String) reader.read(String.class));
                        log.debug("[METADATA] from " + connection + " {" + networkId + ", " + connection.getDeviceId() + "}");
                        updateListeners(Event.CONNECTION_DISCOVERED, connection);
                        monitoredConnections.add(connection);
                    }
                    break;

                case Message.I_QUIT:
                    log.debug("[I_QUIT] from " + connection);
                    onLost(connection);
                    break;

            }
        } catch (InvalidReadException e) {
            log.error("Malformed private stream received!", e);
        }
    }

    private class ConnectionMonitorThread extends Thread {

        private static final int INTERVAL = 5000;
        private static final int FAILURE_THRESHOLD = 5;

        private boolean isRunning;
        private Map<Connection, Integer> sendFailureCount;

        ConnectionMonitorThread() {
            isRunning = false;
            sendFailureCount = new ConcurrentHashMap<>();
        }

        @Override
        public void run() {
            isRunning = true;

            while (isRunning) {

                for (Connection connection : monitoredConnections) {
                    monitor(connection);
                }

                Wait.period(INTERVAL);
            }
        }

        void terminate() {
            isRunning = false;
        }

        private void monitor(Connection connection) {
            try {

                connection.send(Message.monitor());

                // Send has succeeded for this connection
                if (sendPreviouslyFailedFor(connection))
                    restore(connection);

            } catch (NetworkException e) {
                if (!sendPreviouslyFailedFor(connection))
                    onSendFailureTo(connection);
            }
        }

        private void onSendFailureTo(Connection connection) {
            Integer previousCount = sendFailureCount.get(connection);
            sendFailureCount.put(connection, (previousCount == null ? 0 : previousCount) + 1);

            // Report connection as lost when send fails for the first time
            if (previousCount == null || previousCount == 0)
                onLost(connection);

            // Stop monitoring the connection if send fails continuously
            if (previousCount != null && previousCount > FAILURE_THRESHOLD)
                monitoredConnections.remove(connection);
        }

        private boolean sendPreviouslyFailedFor(Connection connection) {
            Integer previousCount = sendFailureCount.get(connection);
            return previousCount != null && previousCount > 0;
        }

        private void restore(Connection connection) {
            sendFailureCount.put(connection, 0);
            onDiscovered(connection);
        }

    }

}
