package com.microsoft.peersense.core;

import com.microsoft.peersense.support.callback.Listenable;

import java.io.InputStream;

/**
 * Created by shubham on 2/21/17.
 */
public abstract class Module extends Listenable<NetworkListener> {

    public abstract void startup();

    public abstract void shutdown();

    protected final void onDiscovered(Connection connection) {
        updateListeners(Event.CONNECTION_DISCOVERED, connection);
    }

    protected final void onLost(Connection connection) {
        updateListeners(Event.CONNECTION_LOST, connection);
    }

    protected final void onDataReceived(final Connection connection, InputStream stream) {
        updateListeners(Event.DATA_RECEIVED, connection, new Payload(stream));
    }

}
