package com.microsoft.peersense.core.exception;

import java.io.IOException;

/**
 * Created by shubham on 9/23/16.
 */
public class RuntimeIOException extends RuntimeException {

    public RuntimeIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuntimeIOException(IOException cause) {
        super(cause);
    }

    public RuntimeIOException(String message) {
        super(message);
    }
}
