package com.microsoft.peersense.core.protocol;

import com.microsoft.peersense.support.stream.ProgressListener;
import com.microsoft.peersense.support.stream.Stream;
import org.apache.commons.lang.SerializationUtils;

import java.io.InputStream;
import java.io.Serializable;

/**
 * Created by shubham on 3/3/17.
 */
public class SerializableProtocol extends Protocol<Serializable> {

    static final int CHUNK = 4096;
    private static final IntegerProtocol integerProtocol = new IntegerProtocol();

    @Override
    protected SerializationResult getSerialized(Serializable object) {

        byte[] bytes = SerializationUtils.serialize(object);
        SerializationResult sObject = build(Stream.build(bytes), (long) bytes.length);
        SerializationResult sLength = integerProtocol.serialize(bytes.length);
        return combine(sLength, sObject);

    }

    @Override
    protected DeserializationResult getDeserialized(InputStream stream, ProgressListener listener) {

        int objectSizeInBytes = (int) integerProtocol.deserialize(stream).getObject();
        byte[] objectByteArray = Stream.read(stream, objectSizeInBytes, CHUNK, listener);
        return build((Serializable) SerializationUtils.deserialize(objectByteArray), (long) objectSizeInBytes);

    }

    @Override
    protected byte getTypeCode() {
        return Type.SERIALIZABLE.getCode();
    }
}
