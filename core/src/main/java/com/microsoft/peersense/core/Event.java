package com.microsoft.peersense.core;

/**
 * Created by shubham on 4/19/17.
 */
public class Event {

    static final int CONNECTION_DISCOVERED = 1;

    static final int CONNECTION_LOST = 2;

    static final int DATA_RECEIVED = 3;

}
