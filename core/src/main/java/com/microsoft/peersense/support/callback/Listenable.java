package com.microsoft.peersense.support.callback;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * Created by shubham on 9/23/16.
 */
public abstract class Listenable<T extends Listener> {

    private Set<T> listeners;

    public Listenable() {
        this.listeners = Sets.newConcurrentHashSet();
    }

    public final void addListener(T listener) {
        listeners.add(listener);
    }

    public final void removeListener(T listener) {
        listeners.remove(listener);
    }

    public final void removeAllListeners() {
        listeners.clear();
    }

    protected final void updateListeners(Object... arguments) {
        for (T listener : listeners) {
            listener.onUpdated(arguments);
        }
    }

}
