package com.microsoft.peersense.support.stream;

import com.microsoft.peersense.core.exception.RuntimeIOException;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by shubham on 4/19/17.
 */
public class Stream {

    private static final int DEFAULT_CHUNK_SIZE = 4096;

    public static long copy(InputStream input, OutputStream output, long totalBytesToCopy) throws IOException {
        return copy(input, output, totalBytesToCopy, DEFAULT_CHUNK_SIZE, ProgressListener.NO_PROGRESS_REPORTING);
    }

    public static long copy(InputStream input, OutputStream output, long totalBytesToCopy, ProgressListener progressListener) throws IOException {
        return copy(input, output, totalBytesToCopy, DEFAULT_CHUNK_SIZE, progressListener);
    }

    public static long copy(InputStream input, OutputStream output, long totalBytesToCopy, int chunkSize, ProgressListener progressListener) throws IOException {
        long totalBytesCopied = 0;

        byte[] chunk = new byte[chunkSize];
        while (totalBytesCopied < totalBytesToCopy) {

            long bytesLeft = totalBytesToCopy - totalBytesCopied;
            int bytesToRead = (bytesLeft < (long) chunkSize) ? (int) bytesLeft : chunkSize;

            int bytesRead = input.read(chunk, 0, bytesToRead);
            if (bytesRead == -1)
                break;

            totalBytesCopied += bytesRead;
            output.write(chunk, 0, bytesRead);

            if (progressListener != null)
                progressListener.onProgress(totalBytesCopied, totalBytesToCopy);
        }

        return totalBytesCopied;
    }

    public static void close(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    public static int available(InputStream source) {
        try {
            return source.available();
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    public static byte[] read(InputStream stream, int numberOfBytes, int chunk) {
        return read(stream, numberOfBytes, chunk, ProgressListener.NO_PROGRESS_REPORTING);
    }

    public static byte[] read(InputStream stream, int numberOfBytes) {
        return read(stream, numberOfBytes, DEFAULT_CHUNK_SIZE);
    }

    public static byte[] read(InputStream stream, int numberOfBytes, int chunk, ProgressListener listener) {
        try {

            ByteArrayOutputStream output = new ByteArrayOutputStream(numberOfBytes);
            copy(stream, output, numberOfBytes, chunk, listener);
            return output.toByteArray();

        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    public static InputStream build(byte... bytes) {
        return new ByteArrayInputStream(bytes);
    }

    public static InputStream attach(InputStream... streams) {
        return new BufferedInputStream(new SequenceInputStream(Collections.enumeration(Arrays.asList(streams))));
    }

    public static InputStream attach(List<InputStream> streams) {
        return attach(streams.toArray(new InputStream[streams.size()]));
    }

}
