package com.microsoft.peersense.support.concurrency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shubham on 3/29/17.
 */
public class Wait {

    private static final Logger log = LoggerFactory.getLogger(Wait.class);

    // Milliseconds
    private static long DEFAULT_TIMEOUT = 600000;
    private static long DEFAULT_ITERATIONS = 600;

    public static void period(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void until(Condition condition) {
        until(condition, DEFAULT_TIMEOUT, DEFAULT_ITERATIONS);
    }

    public static void until(Condition condition, long timeoutInMilliseconds) {
        until(condition, timeoutInMilliseconds, DEFAULT_ITERATIONS);
    }

    public static void until(Condition condition, long timeoutInMilliseconds, long iterations) {
        if (condition == null) return;

        long whenWaitStarted = System.currentTimeMillis();

        log.debug("Waiting until {" + condition.getDescription() + "}...");
        while (System.currentTimeMillis() - whenWaitStarted < timeoutInMilliseconds) {

            if (condition.isSatisfied())
                break;

            Wait.period(timeoutInMilliseconds / iterations);
        }

        if (!condition.isSatisfied())
            throw new RuntimeException("The condition {" + condition.getDescription() + "} was not satisfied in the given timeout");
        else
            log.debug("{" + condition.getDescription() + "} satisfied.");

    }

}
