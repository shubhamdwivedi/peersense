package com.microsoft.peersense.support.callback;

/**
 * Created by shubham on 9/23/16.
 */
public interface Listener {

    void onUpdated(Object... arguments);

}
