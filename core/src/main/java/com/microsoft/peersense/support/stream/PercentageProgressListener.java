package com.microsoft.peersense.support.stream;

import com.microsoft.peersense.support.CommonUtils;

/**
 * Created by shubham on 5/7/17.
 */
public abstract class PercentageProgressListener extends ProgressListener {

    public static final PercentageProgressListener NO_PROGRESS_REPORTING = new PercentageProgressListener() {
        @Override
        public void onPercentageCompleted(float percentageCompleted) {
        }
    };

    @Override
    public void onProgress(long done, long total) {
        onPercentageCompleted(CommonUtils.percentage(done, total));
    }

    public abstract void onPercentageCompleted(float percentageCompleted);

}
