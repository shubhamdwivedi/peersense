package com.microsoft.peersense.support;

import com.microsoft.peersense.support.stream.Stream;

import java.io.*;
import java.lang.reflect.Type;
import java.util.UUID;

/**
 * Created by shubham on 3/28/17.
 */
public class CommonUtils {

    public static Type[] getTypeArray(Object[] objects) {
        Type[] types = new Type[objects.length];

        for (int i = 0; i < objects.length; i++)
            types[i] = objects[i].getClass();

        return types;
    }

    public static String tempDir(String name) {
        String dir = System.getProperty("java.io.tmpdir") + "com.microsoft.peersense" + File.separator + name + File.separator;
        new File(dir).mkdirs();
        return dir;
    }

    public static File createTemporaryFile(long sizeInBytes) {
        File temp = new File(CommonUtils.tempDir("temporary_files") + UUID.randomUUID().toString() + ".txt");
        try {

            InputStream input = new RandomInputStream();
            OutputStream output = new FileOutputStream(temp);
            Stream.copy(input, output, sizeInBytes);
            input.close();
            output.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp;
    }

    public static float percentage(float actual, float total) {
        return round((actual * 100f) / total, 2);
    }

    public static float round(float number, int numberOfDecimalPlaces) {
        return new Float(String.format("%." + numberOfDecimalPlaces + "f", number));
    }

    private static class RandomInputStream extends InputStream {
        @Override
        public int read() throws IOException {
            return (int) (Math.random() * 10);
        }
    }

}
