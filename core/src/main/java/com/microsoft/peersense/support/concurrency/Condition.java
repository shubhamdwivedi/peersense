package com.microsoft.peersense.support.concurrency;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by shubham on 3/29/17.
 */
@AllArgsConstructor
public abstract class Condition {

    @Getter
    private String description;

    public abstract boolean isSatisfied();

}
