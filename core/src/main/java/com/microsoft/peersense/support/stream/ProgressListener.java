package com.microsoft.peersense.support.stream;

import com.microsoft.peersense.support.callback.Listener;

/**
 * Created by shudwi on 3/2/2017.
 */
public abstract class ProgressListener implements Listener {

    public static final ProgressListener NO_PROGRESS_REPORTING = new ProgressListener() {
        @Override
        public void onProgress(long unitsDone, long totalUnits) {
        }
    };

    @Override
    public final void onUpdated(Object... arguments) {
        onProgress((long) arguments[0], (long) arguments[1]);
    }

    public abstract void onProgress(long unitsDone, long totalUnits);

}
