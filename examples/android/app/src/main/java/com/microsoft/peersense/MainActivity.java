package com.microsoft.peersense;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import com.microsoft.peersense.core.FunctionalTest;
import com.microsoft.peersense.core.Network;
import com.microsoft.peersense.modules.jmdns.JmDNSModule;

import org.apache.log4j.BasicConfigurator;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    static {
        BasicConfigurator.configure();
    }

    private Network network;
    private WifiManager.MulticastLock multicastLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Thread(new Runnable() {
            @Override
            public void run() {
                main();
            }
        }).start();
    }

    private void main() {

        network = Network.getInstance();
        network.setNetworkId("private_network");
        acquireMulticastLock();
        network.install(JmDNSModule.getInstance());
        new FunctionalTest().run();

    }

    private void acquireMulticastLock() {
        WifiManager wifi = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        multicastLock = wifi.createMulticastLock(getClass().getName());
        multicastLock.setReferenceCounted(true);
        multicastLock.acquire();
        try {
            String ip = Formatter.formatIpAddress(wifi.getConnectionInfo().getIpAddress());
            JmDNSModule.getInstance().setLocalAddress(InetAddress.getByName(ip));
        }
        catch (UnknownHostException e) {}
    }

}
