package com.microsoft.peersense;

import com.microsoft.peersense.core.*;
import com.microsoft.peersense.modules.jmdns.JmDNSModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shubham on 4/18/17.
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        Network network = Network.getInstance();
        network.setNetworkId("private_network");
        network.install(JmDNSModule.getInstance());
        new FunctionalTest().run();

    }

}
